import * as dotenv from 'dotenv';
import 'reflect-metadata';
import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { Container } from 'typedi';
import * as TypeGraphQL from 'type-graphql';

dotenv.config();

// ...

import pubSub from './src/integrations/pubSub';

async function bootstrap() {
    const app = express();
    const port: string = process.env.APP_PORT;

    // ...

    await pubSub.listening();

    // ...

    const schema = await TypeGraphQL.buildSchema({
        resolvers: Object.values(require('./src/resolvers')),
        container: Container,
        emitSchemaFile: { path: 'schema.graphql' },
        authChecker,
    });

    const server = new ApolloServer({
        schema,
        context: ({ req }: { req: IRequest }) => {
            const ctx: IContext = { user: req.user, req };
            return ctx;
        },
        uploads: {
            maxFileSize: 25000000, // 25MB in bytes
        },
    });

    server.applyMiddleware({ app, cors: false });

    app.listen(port, () => {
        console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
    });
}

bootstrap();
