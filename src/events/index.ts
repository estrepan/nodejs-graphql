// ...

export enum Hubspot {
    CONTACT_ACTIVATE = 'HubspotContactActivate',
    CONTACT_CREATE = 'HubspotContactCreate',
    CONTACT_DEACTIVATE = 'HubspotContactDeactivate',
    CONTACT_UPDATE_SUBSCRIPTION = 'HubspotContactUpdateSubscription',
}

export enum Mailer {
    USER_APPROVED_SEND = 'SendgridUserApprovedSend',
    USER_APPROVED_WITH_PAYMENT_SEND = 'SendgridUserApprovedWithPaymentSend',
    USER_REGISTERED_SEND = 'SendgridUserRegisteredSend',
    USER_REJECTED_SEND = 'SendgridUserRejectedSend',
    USER_DISABLED_SEND = 'SendgridUserDisabledSend',
    USER_INVITED_SEND = 'SendgridUserInvitedSend',
    BUNDLE_PAUSED_SEND = 'SendgridBundlePausedSend',
    BUNDLE_REOPENED_SEND = 'SendgridBundleReopenedSend',
    BUNDLE_NOTIFICATION_SEND = 'SendgridBundleNotificationSend',
    BUNDLE_WITHDRAWN_SEND = 'SendgridBundleWithdrawnSend',
    BID_SUBMITTED_SEND = 'SendgridBidSubmittedSend',
    BID_REOPENED_SEND = 'SendgridBidReopenedSend',
    BID_CONTRACTED_SEND = 'SendgridBidContractedSend',
    BID_REJECTED_SEND = 'SendgridBidRejectedSend',
    BID_INVITED_SEND = 'SendgridBidInvitedSend',
    BID_INVITED_GHOST_SEND = 'SendgridBidInvitedGhostSend',
    BID_DOCUMENTS_REQUESTED_SEND = 'SendgridBidDocumentsRequestedSend',
}

export enum Slack {
    USER_REGISTERED_NOTIFY = 'SlackUserRegisteredNotify',
    USER_INVITED_NOTIFY = 'SlackUserInvitedNotify',
    USER_SUBSCRIBED_NOTIFY = 'SlackUserSubscribedNotify',
    BID_INVITED_GHOST_NOTIFY = 'SlackBidInvitedNotify',
}

// ...
