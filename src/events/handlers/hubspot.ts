import { Stripe } from 'stripe';
import { Event } from '../../types/events';
import { Hubspot as HubspotEvent } from '..';
import { Organization, UserApproval } from '../../entities';
import { Options } from '../../types/pubSub';
import { IUser } from '../../types/user';
import Hubspot from '../../integrations/hubspot';
import logger from '../../common/logger';

export const HubspotEvents = <Event>{
    [HubspotEvent.CONTACT_ACTIVATE]: async (
        data: { user: IUser, approval: UserApproval },
        options: Options,
    ): Promise<void> => {
        await Hubspot.activateContactAndCompany(data.user, data.approval).catch(err => logger.error(err));
    },
    [HubspotEvent.CONTACT_CREATE]: async (
        data: { user: IUser, organization: Organization },
        options: Options,
    ): Promise<void> => {
        await Hubspot.createContactAndCompany(data.user, data.organization).catch(err => logger.error(err));
    },
    [HubspotEvent.CONTACT_DEACTIVATE]: async (
        data: { user: IUser },
        options: Options,
    ): Promise<void> => {
        await Hubspot.deactivateContact(data.user.login_email).catch(err => logger.error(err));
    },
    [HubspotEvent.CONTACT_UPDATE_SUBSCRIPTION]: async (
        data: { user: IUser, subscription: Stripe.Subscription },
        options: Options,
    ): Promise<void> => {
        await Hubspot.updateSubscription(data.user, data.subscription).catch(err => logger.error(err));
    },
};
