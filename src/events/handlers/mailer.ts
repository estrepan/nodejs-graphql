import { Event } from '../../types/events';
import { Mailer as MailerEvent } from '..';
import { Options } from '../../types/pubSub';
import Mailer from '../../common/mailer';
import { IUser } from '../../types/user';
import { Organization, UserInvite } from '../../entities';

export const MailerEvents = <Event>{
    [MailerEvent.USER_APPROVED_SEND]: async (
        data: { user: IUser },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserApprovedEmail(data.user, options);
    },
    [MailerEvent.USER_APPROVED_WITH_PAYMENT_SEND]: async (
        data: { user: IUser },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserApprovedWithPaymentEmail(data.user, options);
    },
    [MailerEvent.USER_REGISTERED_SEND]: async (
        data: { user: IUser, organization: Organization },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserRegisteredEmail(data, options);
    },
    [MailerEvent.USER_REJECTED_SEND]: async (
        data: { user: IUser, rejection_note: string },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserRejectedEmail(data, options);
    },
    [MailerEvent.USER_DISABLED_SEND]: async (
        data: { user: IUser },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserDisabledEmail(data.user, options);
    },
    [MailerEvent.USER_INVITED_SEND]: async (
        data: { invites: UserInvite[], invitedBy: IUser },
        options: Options,
    ): Promise<void> => {
        await Mailer.sendUserInvitedEmails(data, options);
    },
};
