import { Event } from '../../types/events';
import { Slack as SlackEvent } from '..';
import { Options } from '../../types/pubSub';
import { IUser } from '../../types/user';
import { postToSlack } from '../../integrations/slack';
import { UserInvite, User } from '../../entities';

export const SlackEvents = <Event>{
    [SlackEvent.USER_REGISTERED_NOTIFY]: async (
        data: { user: IUser },
        options: Options,
    ): Promise<void> => {
        const { user } = data;
        const slackMsg = `:man-raising-hand: ${user.first_name} ${user.last_name} (${user.login_email})`
            + ' requested access.';

        await postToSlack(process.env.SLACK_WEBHOOK_URL, slackMsg);
    },
    [SlackEvent.USER_INVITED_NOTIFY]: async (
        data: { invites: UserInvite[], invitedBy: IUser },
        options: Options,
    ): Promise<void> => {
        const { invites, invitedBy } = data;
        const emails = invites.map(invitation => invitation.email);
        const slackMsg = `:heavy_plus_sign: ${invitedBy.first_name} ${invitedBy.last_name} invited people`
            + ` to ${invitedBy.organization.name} - ${emails.join(', ')}`;

        await postToSlack(process.env.SLACK_WEBHOOK_URL, slackMsg);
    },
    [SlackEvent.USER_SUBSCRIBED_NOTIFY]: async (
        data: { user: IUser, orgOwner: User | null },
        options: Options,
    ): Promise<void> => {
        const { user, orgOwner } = data;

        const slackMsg = user.login_as_id
            ? `:moneybag: ${user.first_name} ${user.last_name} subscribed`
                + ` ${orgOwner.first_name} ${orgOwner.last_name} (${orgOwner.login_email})`
                + ` from ${orgOwner.organization.name}!`
            : `:moneybag: ${user.first_name} ${user.last_name} (${user.login_email})`
                + ` from ${user.organization.name} subscribed!`;

        await postToSlack(process.env.SLACK_WEBHOOK_URL, slackMsg);
    },
    [SlackEvent.BID_INVITED_GHOST_NOTIFY]: async (
        data: { invite: UserInvite },
        options: Options,
    ): Promise<void> => {
        const { invite } = data;
        const slackMsg = `:heavy_plus_sign: Ghost SP - ${invite.email} was invited to `
            + `a bundle from ${invite.organization.name}.`;

        await postToSlack(process.env.SLACK_WEBHOOK_URL, slackMsg);
    },
};
