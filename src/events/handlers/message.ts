import { getRepository, Not } from 'typeorm';
import { Event } from '../../types/events';
import { Message as MessageEvent } from '..';
import { Message, User } from '../../entities';
import { Options } from '../../types/pubSub';
import twilio from '../../integrations/twilio';


export const MessageEvents = <Event>{
    [MessageEvent.MESSAGE_SENT]: async (
        data: { message: Message, fromUser: User, toUser: User },
        options: Options,
    ): Promise<void> => {
        const { fromUser, toUser } = data;
        const message = await getRepository(Message).findOneOrFail({ message_id: data.message.message_id });
        const lastMessage = await getRepository(Message).findOne({
            where: [{
                from_user_id: message.from_user_id,
                to_user_id: message.to_user_id,
                message_id: Not(message.message_id),
            }, {
                from_user_id: message.to_user_id,
                to_user_id: message.from_user_id,
                message_id: Not(message.message_id),
            }],
            order: {
                sent_on: 'DESC',
            },
        });
        if (!lastMessage?.twilio_session_id) {
            const session = await twilio.createSession(fromUser, toUser);
            lastMessage.twilio_session_id = session.sid;
        }
        const sms = await twilio.sendSessionMessage(lastMessage.twilio_session_id, toUser, message.body);
        await getRepository(Message).update(
            {
                message_id: message.message_id,
            }, {
                twilio_session_id: lastMessage.twilio_session_id,
                twilio_id: sms.sid,
            },
        );
    },
};
