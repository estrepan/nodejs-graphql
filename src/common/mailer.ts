import Sendgrid from '../integrations/sendgrid';
import { MailResponse, MailerOptions, MailDataRequired } from '../types/mailer';
import { IUser } from '../types/user';
import { Emails, Templates } from '../constants/mailer';
import { Organization, UserInvite } from '../entities';
import { ACCEPT_INVITATION_URL } from '../constants/urls';
import logger from './logger';

const root_url = process.env.UI_ROOT_URL || 'https://app-dev.procursys.com/#';

class Mailer {
    private sg = Sendgrid;

    constructor() {
        this.sg = Sendgrid;
    }

    public async sendUserRegisteredEmail(
        data: { user: IUser, organization: Organization },
        options?: MailerOptions,
    ): Promise<MailResponse> {
        const template = data.organization.is_facility_manager
            ? Templates.USER_FM_REGISTERED : Templates.USER_SP_REGISTERED;
        const templateData = {
            first_name: data.user.first_name,
            login_email: data.user.login_email,
        };
        const msg = this.resolveMsg(data.user.login_email, Emails.CONNECT, template, templateData);
        return this.send(msg, options);
    }

    public async sendUserApprovedEmail(data: IUser, options?: MailerOptions): Promise<MailResponse> {
        const msg = this.resolveMsg(data.login_email, Emails.MEMBERS, Templates.USER_APPROVED, data);
        return this.send(msg, options);
    }

    public async sendUserApprovedWithPaymentEmail(data: IUser, options?: MailerOptions): Promise<MailResponse> {
        const msg = this.resolveMsg(data.login_email, Emails.MEMBERS, Templates.USER_SP_APPROVED_WITH_PAYMENT, data);
        return this.send(msg, options);
    }

    public async sendUserRejectedEmail(
        data: { user: IUser, rejection_note: string },
        options?: MailerOptions,
    ): Promise<MailResponse> {
        const templateData = {
            first_name: data.user.first_name,
            rejection_note: data.rejection_note,
        };
        const msg = this.resolveMsg(data.user.login_email, Emails.CONNECT, Templates.USER_REJECTED, templateData);
        return this.send(msg, options);
    }

    public async sendUserDisabledEmail(data: IUser, options?: MailerOptions): Promise<MailResponse> {
        const msg = this.resolveMsg(data.login_email, Emails.CONNECT, Templates.USER_DISABLED, data);
        return this.send(msg, options);
    }

    public async sendUserInvitedEmails(
        data: {
            invites: UserInvite[],
            invitedBy: IUser
        },
        options?: MailerOptions,
    ): Promise<MailResponse> {
        const msg = data.invites.map(invitation => this.resolveMsg(
            invitation.email,
            Emails.MEMBERS,
            Templates.USER_INVITED,
            {
                administrator_name: data.invitedBy.first_name,
                organization: data.invitedBy.organization.name,
                invitation_url: `${ACCEPT_INVITATION_URL}/${invitation.invite_id}`,
            },
        ));
        return this.send(msg, options);
    }

    private resolveMsg(to: string, from: string, templateId: string, data: object): MailDataRequired {
        logger.log('Send message', { to, from, templateId });
        return {
            to,
            from,
            templateId,
            dynamicTemplateData: { ...data, root_url },
            content: [{
                type: 'text/html',
                value: 'email',
            }],
        };
    }

    private async send(data: MailDataRequired | MailDataRequired[], options?: MailerOptions): Promise<MailResponse> {
        if (options.mail_sending_enabled) {
            return this.sg.sendEmail(data);
        }
        return false;
    }
}

export default new Mailer();
