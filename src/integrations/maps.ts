import { Client } from '@googlemaps/google-maps-services-js';
import {
    GeocodeRequest,
    ReverseGeocodeRequest,
    GeocodeResponse,
    ReverseGeocodeResponse,
} from '@googlemaps/google-maps-services-js/dist/client';

const client = new Client({});

declare interface IMaps {
    geocode: (
        params: object
    ) => Promise<GeocodeResponse>;
    reverseGeocode: (
        params: object
    ) => Promise<ReverseGeocodeResponse>;
}

export class Maps implements IMaps {
    private client: any;

    constructor(private readonly apiKey: string) {
        this.client = new Client({});
        this.apiKey = apiKey;
    }

    async geocode(params: object): Promise<GeocodeResponse> {
        return client.geocode({ params: { ...params, key: this.apiKey } } as GeocodeRequest);
    }

    async reverseGeocode(params: object): Promise<ReverseGeocodeResponse> {
        return client.reverseGeocode({ params: { ...params, key: this.apiKey } } as ReverseGeocodeRequest);
    }
}

export default new Maps(process.env.GOOGLE_MAPS_API_KEY);
