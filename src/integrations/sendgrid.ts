import { MailService } from '@sendgrid/mail';
import { ResponseError } from '@sendgrid/helpers/classes';
import { MailResponse, MailDataRequired } from '../types/mailer';
import logger from '../common/logger';

declare interface ISendgridCallback {
    (err: Error | ResponseError, result: MailResponse): void
}
declare interface ISendgrid {
    sendEmail: (
        data: MailDataRequired | MailDataRequired[],
        cb?: ISendgridCallback
    ) => Promise<MailResponse>;
}

export class Sendgrid implements ISendgrid {
    private sgMail = new MailService();

    constructor(private sendgridApiKey: string) {
        this.sgMail = new MailService();
        this.sgMail.setApiKey(sendgridApiKey);
    }

    public async sendEmail(
        data: MailDataRequired | MailDataRequired[], callback?: ISendgridCallback,
    ): Promise<MailResponse> {
        // @ts-ignore
        return this.sgMail.send(data, Array.isArray(data), callback)
            .catch(error => {
                logger.error(error);
            });
    }
}

export default new Sendgrid(process.env.SENDGRID_API_KEY);
