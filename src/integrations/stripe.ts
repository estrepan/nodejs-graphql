import StripeConstructor, { Stripe } from 'stripe';
import { Container } from 'typedi';

export const stripe = new StripeConstructor(process.env.STRIPE_API_KEY, {
    apiVersion: '2020-03-02',
    typescript: true,
});
Container.set(StripeConstructor, stripe);

/**
 * "stripe" is instance of api
 * "Stripe" is namespace
 * "StripeConstructor" is constructor/class
 */
export { StripeConstructor, Stripe };
