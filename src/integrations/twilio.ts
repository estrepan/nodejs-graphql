import * as twilio from 'twilio';
import { find } from 'lodash';
import logger from '../common/logger';
import { User } from '../entities';

const proxyServiceSid = process.env.TWILIO_PROXY_SERVICE;

class Twilio {
    private client: twilio.Twilio;

    constructor() {
        try {
            this.client = twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
        } catch (e) {
            logger.debug(`Twilio Error: ${e.message}`);
        }
    }

    async createSession(from: User, to: User) {
        const session = await this.client.proxy.services(proxyServiceSid)
            .sessions
            .create();
        await Promise.all([
            this.client.proxy.services(proxyServiceSid)
                .sessions(session.sid)
                .participants
                .create({ friendlyName: from.first_name, identifier: from.phone_number }),
            this.client.proxy.services(proxyServiceSid)
                .sessions(session.sid)
                .participants
                .create({ friendlyName: to.first_name, identifier: to.phone_number }),
        ]);
        return session;
    }

    async sendSessionMessage(sessionId: string, toUser: User, body: string) {
        const participant = await this.getParticipantByPhoneNumber(sessionId, toUser);
        return this.client.proxy.services(proxyServiceSid)
            .sessions(sessionId)
            .participants(participant.sid)
            .messageInteractions
            .create({ body });
    }

    async getParticipantByPhoneNumber(sessionId: string, toUser: User) {
        return this.client.proxy.services(proxyServiceSid)
            .sessions(sessionId)
            .participants
            .list()
            .then(participants => find(participants, ['identifier', toUser.phone_number]));
    }
}

export default new Twilio();
