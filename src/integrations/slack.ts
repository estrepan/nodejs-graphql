import { IncomingWebhook, IncomingWebhookResult } from '@slack/webhook';
import * as Sentry from '@sentry/node';
import logger from '../common/logger';

export class SlackError extends Error {
    message: string;

    originalError?: Error;

    constructor(message: string, originalError?: Error) {
        super(message);
        this.message = message;
        this.originalError = originalError;
    }
}

export async function postToSlack(webhookUrl: string, message: string): Promise<IncomingWebhookResult | null> {
    try {
        const webhook = new IncomingWebhook(webhookUrl);
        return webhook.send(message);
    } catch (exception) {
        const errorMessage = `Failed posting to Slack: ${JSON.stringify({ webhookUrl, message })}`;
        const error = new SlackError(errorMessage, exception);
        logger.error(error);
        Sentry.captureException(error);
        return null;
    }
}
