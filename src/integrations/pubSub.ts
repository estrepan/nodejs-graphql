import { PubSub } from '@google-cloud/pubsub';
import { Attributes } from '@google-cloud/pubsub/build/src/publisher';
import * as os from 'os';
import { forEach } from 'lodash';
import { EventCallback } from '../types/events';
import { Client, Topic, Options } from '../types/pubSub';
import logger from '../common/logger';
import { DEVELOPMENT } from '../constants/environments';
import * as events from '../events/handlers';

const messageOptions = <Options>{
    retry_count: 0,
    mail_sending_enabled: process.env.MAIL_SENDING_ENABLED
        ? process.env.MAIL_SENDING_ENABLED === 'true'
        : true,
};
const retryLimit = process.env.GOOGLE_PUBSUB_RETRY_LIMIT || 3;
const resolveEnv = (name: string): string => {
    const env = process.env.NODE_ENV || DEVELOPMENT;
    const envName = process.env.HOST_NAME || os.hostname();
    return `${env}.${envName}.${name}`;
};

export class PubSubClient implements Client {
    private client: PubSub;

    private readonly attributes: Attributes;

    constructor() {
        this.client = new PubSub();
        this.attributes = { options: JSON.stringify(messageOptions) };
    }

    public async publish(event: string, data: object, options?: Options): Promise<string> {
        const eventPrefixed = resolveEnv(event);
        const attributes = options
            ? { options: JSON.stringify(options) }
            : this.attributes;
        return this.client.topic(eventPrefixed).publishJSON(data, attributes);
    }

    private async getAllTopics(): Promise<Topic[]> {
        const topics = await this.client.getTopics();
        return topics[0];
    }

    private async getAllTopicsNames(): Promise<string[]> {
        const topics = await this.getAllTopics();
        return topics.map(topic => topic.name.split('/').pop());
    }

    private async createTopic(name: string): Promise<void> {
        await this.client.createTopic(name);
    }

    private async createSubscription(topic: string): Promise<void> {
        await this.client.topic(topic).createSubscription(topic);
    }

    private createListener(event: string, callback: EventCallback): void {
        this.client.subscription(resolveEnv(event), {
            streamingOptions: {
                maxStreams: 1,
            },
        })
            .on('message', (message) => {
                message.ack();

                const data = JSON.parse(message.data.toString());
                const options = JSON.parse(message.attributes.options);

                callback(data, options)
                    .catch((error) => {
                        logger.error(error);
                        options.retry_count += 1;
                        if (options.retry_count <= retryLimit) {
                            this.publish(event, data, options);
                        }
                    });
            })
            .on('error', err => logger.error(err));
    }

    public async listening(): Promise<void> {
        const topics = await this.getAllTopicsNames();
        const newTopics = [];
        const listeners = [];
        forEach(events, (list) => {
            forEach(list, (callback, event) => {
                const topic = resolveEnv(event);
                if (!topics.includes(topic)) {
                    newTopics.push(topic);
                }
                listeners.push({
                    event,
                    callback,
                });
            });
        });
        await Promise.all(
            newTopics.map(topic => this.createTopic(topic)),
        );
        await Promise.all(
            newTopics.map(topic => this.createSubscription(topic)),
        );
        await Promise.all(listeners.map(
            listener => this.createListener(listener.event, listener.callback),
        ));
    }
}

export default new PubSubClient();
