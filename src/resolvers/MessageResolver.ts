import {
    Arg, Args, Ctx, Field, Int, Mutation, ObjectType, Query, Resolver, UseMiddleware,
} from 'type-graphql';
import { Connection, In, Repository } from 'typeorm';
import { InjectConnection, InjectRepository } from 'typeorm-typedi-extensions';
import { Message, User } from '../entities';
import { BaseAllArgs, createCrudResolver } from '../common/CrudResolver';
import { IContext } from '../types/common';
import { allowOnlyLoggedIn, allowWithPermissions } from '../middleware/auth';
import pubSub from '../integrations/pubSub';
import { Message as MessageEvent } from '../events';
import { Permission } from '../constants/permissions';

const BaseMessageResolver = createCrudResolver(
    Message,
    {
        delete: { disabled: true },
        create: { disabled: true },
        update: { disabled: true },
    },
);

@ObjectType()
class Conversation extends User {
    @Field()
    unread: number;
}

@Resolver(of => Message)
export default class MessageResolver extends BaseMessageResolver {
    @InjectConnection()
    protected readonly connection: Connection;

    @InjectRepository(Message)
    protected readonly messageRepository: Repository<Message>;

    @InjectRepository(User)
    protected readonly userRepository: Repository<User>;

    @Mutation(type => Message)
    @UseMiddleware(allowWithPermissions([Permission.SEND_MESSAGE]))
    async sendMessage(
        @Ctx() { user }: IContext,
        @Arg('to_user_id', returns => Int) toUserId: number,
        @Arg('body') body: string,
        @Arg('sent_from', returns => String, { nullable: true }) sentFrom?: string,
    ) {
        const toUser = await this.connection.getRepository(User).findOneOrFail(toUserId);
        const message = await this.messageRepository.save(this.messageRepository.create(<Message>{
            from_user_id: user.user_id,
            to_user_id: toUser.user_id,
            body,
            sent_on: new Date(),
            sent_from: sentFrom,
        }));

        await pubSub.publish(MessageEvent.MESSAGE_SENT, { message, fromUser: user, toUser });

        return message;
    }

    @Query(returns => [Conversation])
    @UseMiddleware(allowOnlyLoggedIn)
    async getMessageThreadsList(
        @Ctx() { user }: IContext,
        @Args(returns => BaseAllArgs) args: BaseAllArgs,
    ): Promise<Conversation[]> {
        return this.userRepository
            .createQueryBuilder('users')
            .select(['users.*', 'IFNULL(mm.unread, 0) as unread'])
            .innerJoin(subQuery => subQuery
                .select('m.*')
                .from(Message, 'm')
                .leftJoin(
                    Message,
                    'm1',
                    '((m.from_user_id  = m1.from_user_id AND m.to_user_id  = m1.to_user_id) '
                    + 'OR (m.from_user_id  = m1.to_user_id  AND m.to_user_id  = m1.from_user_id)) '
                    + 'AND m.sent_on < m1.sent_on',
                )
                .where(
                    'm1.message_id IS NULL and (m.`from_user_id` = :userId OR m.`to_user_id` = :userId)',
                    { userId: user.user_id },
                )
                .orderBy('m.message_id'),
            'threads',
            `users.user_id <> ${user.user_id} AND (users.user_id = threads.from_user_id 
                OR users.user_id = threads.to_user_id)`)
            .leftJoin(subQuery => subQuery
                .select('from_user_id, count(*) as unread')
                .from(Message, 'messages')
                .where(
                    '`messages`.`to_user_id` = :userId AND `messages`.`is_seen` = :isSeen',
                    { userId: user.user_id, isSeen: 0 },
                )
                .groupBy('from_user_id, to_user_id'),
            'mm',
            'users.user_id = mm.from_user_id')
            .orderBy('threads.sent_on', 'DESC')
            .getRawMany();
    }

    @Query(returns => Int)
    @UseMiddleware(allowOnlyLoggedIn)
    async getUnreadMessagesCount(
        @Ctx() { user }: IContext,
        @Arg('from_user_id', returns => Int, { nullable: true }) fromUserId: number,
    ): Promise<number> {
        const query = this.messageRepository.createQueryBuilder()
            .where({
                to_user_id: user.user_id,
                is_seen: 0,
            });
        if (fromUserId) {
            query.andWhere('from_user_id = :fromUserId', { fromUserId });
        }
        return query.getCount();
    }

    @Mutation(type => Boolean)
    @UseMiddleware(allowOnlyLoggedIn)
    async setMessagesAsSeen(
        @Ctx() { user }: IContext,
        @Arg('from_user_id', returns => Int) fromUserId: number,
        @Arg('messages_ids', returns => [Int]) messagesIds: number[],
    ): Promise<boolean> {
        await this.messageRepository.update(
            { message_id: In(messagesIds), from_user_id: fromUserId, to_user_id: user.user_id },
            { is_seen: true },
        );

        return true;
    }
}
