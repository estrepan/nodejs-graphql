## Description

In this project, I worked on the backend and was mainly involved 
in integrating third-party services. Apart from that, I have worked on 
building GraphQL queries and migrations.

List of third-party services integrated by me:
* Google pub/sub
* Google geocode
* Twilio
* Hubspot
* Sendgrid
* Slack
* Stripe

I provide an example of a pub/sub system, few integrations I have implemented and multiple GraphQL queries and migrations.
Of course I cut out most of the code
